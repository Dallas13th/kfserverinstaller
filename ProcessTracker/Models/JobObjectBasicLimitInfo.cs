﻿using System;
using System.Runtime.InteropServices;
using ProcessTracker.Enums;

namespace ProcessTracker.Models
{
    [StructLayout(LayoutKind.Sequential)]
    internal struct JobObjectBasicLimitInfo
    {
        public Int64 PerProcessUserTimeLimit;
        public Int64 PerJobUserTimeLimit;
        public JobObjectLimit LimitFlags;
        public UIntPtr MinimumWorkingSetSize;
        public UIntPtr MaximumWorkingSetSize;
        public UInt32 ActiveProcessLimit;
        public Int64 Affinity;
        public UInt32 PriorityClass;
        public UInt32 SchedulingClass;
    }
}