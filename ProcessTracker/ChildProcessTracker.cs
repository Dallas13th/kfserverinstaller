﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using ProcessTracker.Enums;
using ProcessTracker.Models;

namespace ProcessTracker
{
    public static class ChildProcessTracker
    {
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
        private static extern IntPtr CreateJobObject(IntPtr lpJobAttributes, string name);

        [DllImport("kernel32.dll")]
        private static extern bool SetInformationJobObject(IntPtr job, JobObjectInfoTypes infoType,
            IntPtr lpJobObjectInfo, uint cbJobObjectInfoLength);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool AssignProcessToJobObject(IntPtr job, IntPtr process);

        private static readonly IntPtr SJobHandle;

        static ChildProcessTracker()
        {
            if (Environment.OSVersion.Version < new Version(6, 2))
                return;

            string jobName = "ChildProcessTracker" + Process.GetCurrentProcess().Id;

            SJobHandle = CreateJobObject(IntPtr.Zero, jobName);

            var info = new JobObjectBasicLimitInfo
            {
                LimitFlags = JobObjectLimit.JobObjectLimitKillOnJobClose
            };

            var extendedInfo = new JobObjectExtendedLimitInformation
            {
                BasicLimitInfo = info
            };

            int length = Marshal.SizeOf(typeof(JobObjectExtendedLimitInformation));

            var extendedInfoPtr = Marshal.AllocHGlobal(length);

            try
            {
                Marshal.StructureToPtr(extendedInfo, extendedInfoPtr, false);

                if (!SetInformationJobObject(SJobHandle, JobObjectInfoTypes.ExtendedLimitInformation,
                    extendedInfoPtr, (uint)length))
                {
                    throw new Win32Exception();
                }
            }
            finally
            {
                Marshal.FreeHGlobal(extendedInfoPtr);
            }
        }

        public static void AddProcess(Process process)
        {
            if (SJobHandle == IntPtr.Zero) return;

            bool success = AssignProcessToJobObject(SJobHandle, process.Handle);
            if (!success)
                throw new Win32Exception();
        }
    }
}