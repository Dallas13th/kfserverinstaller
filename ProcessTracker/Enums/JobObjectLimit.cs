﻿using System;

namespace ProcessTracker.Enums
{
    [Flags]
    internal enum JobObjectLimit : uint
    {
        JobObjectLimitKillOnJobClose = 0x2000
    }
}