﻿namespace ProcessTracker.Enums
{
    internal enum JobObjectInfoTypes
    {
        AssociateCompletionPortInformation = 7,
        BasicLimitInformation = 2,
        BasicUiRestrictions = 4,
        EndOfJobTimeInformation = 6,
        ExtendedLimitInformation = 9,
        SecurityLimitInformation = 5,
        GroupInformation = 11
    }
}