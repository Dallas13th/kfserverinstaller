# README #

#For project compilation make sure you have installed:
* Latest [Visual studio 2017](https://www.visualstudio.com/)
* [.Net Framework 4.7.1 SDK](https://www.microsoft.com/en-us/download/details.aspx?id=56119)

#How to use installer:
* Compile project;
* Run application by following command:
```bat
KfServerInstaller install -d %KfServerRootDirectory%
```
where %KfServerRootDirectory% - your specified path, for example: D:\Kf2ServerRootDirectory\
* Run once kf server for generate default config files;
```bat
%KfServerRootDirectory%\ServerControlScripts\Run-Server-Default.bat
```
* Configure server using following command:
```bat
KfServerInstaller configure -d %KfServerRootDirectory%
```
* Run server again. Don't forget to change default password, using credentials from console output of kfserverinstaller.