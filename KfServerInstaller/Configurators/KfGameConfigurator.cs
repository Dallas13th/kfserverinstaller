﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using KfServerInstaller.Tools;
using Serilog;
using Serilog.Core;

namespace KfServerInstaller.Configurators
{
    public sealed class KfGameConfigurator : IDisposable
    {
        private readonly PathHelper _paths;
        private readonly Logger _logger;

        public KfGameConfigurator(string kfServerRootFolder)
        {
            _logger = new LoggerConfiguration()
                .WriteTo
                .Console()
                .CreateLogger();

            _paths = new PathHelper(kfServerRootFolder, null);
        }

        public void Dispose()
        {
            _logger?.Dispose();
        }

        public void Configure()
        {
            if (!IsValid())
                return;
            
            SetCustomKfWebSettings();
            SetCustomKfEngineSettings();
            SetCustomKfGameSettings();

            _logger.Information("Server Successfully configured!");
        }

        private bool IsValid()
        {
            var validationMessages = ValidateConfigFilesExisting()
                .ToList();

            if (!validationMessages.Any())
                return true;

            foreach (var validationMessage in validationMessages)
            {
                _logger.Error(validationMessage);
            }

            const string suggestionMessage = "Troubleshooting suggestions:\n" +
                                             "    - Make sure you specified correct path to server root folder, which you specified when installing;\n" +
                                             "    - If your server installed, but never started - run once for generate default configs\n" +
                                             "    - If you don't installed server yet - install first";

            _logger.Information(suggestionMessage);

            return false;
        }

        private IEnumerable<string> ValidateConfigFilesExisting()
        {
            if (!File.Exists(_paths.KfWebIniPath))
            {
                yield return CreateValidationMessage(_paths.KfWebIniPath);
            }

            if (!File.Exists(_paths.KfEngineIniPath))
            {
                yield return CreateValidationMessage(_paths.KfEngineIniPath);
            }

            if (!File.Exists(_paths.KfGameIniPath))
            {
                yield return CreateValidationMessage(_paths.KfGameIniPath);
            }
        }

        private string CreateValidationMessage(string filePath)
        {
            var message = $"Config file {Path.GetFileName(filePath)}"
                          + $" didnt not found by path {filePath}";

            return message;
        }

        private void SetCustomKfWebSettings()
        {
            var fileName = Path.GetFileName(_paths.KfWebIniPath);

            LogConfiguring(fileName);
            
            var editor = new IniEditor(_paths.KfWebIniPath);
            SetValue(editor, "IpDrv.WebServer", "bEnabled", "true");

            LogConfigured(fileName);
        }

        private void SetCustomKfEngineSettings()
        {
            var fileName = Path.GetFileName(_paths.KfEngineIniPath);

            LogConfiguring(fileName);
            
            var editor = new IniEditor(_paths.KfEngineIniPath);
            SetValue(editor, "Engine.GameEngine", "bUsedForTakeover", "FALSE");
            SetValue(editor, "Engine.DemoRecDriver", "NetServerMaxTickRate", "60");
            SetValue(editor, "Engine.DemoRecDriver", "LanServerMaxTickRate", "60");
            SetValue(editor, "IpDrv.TcpNetDriver", "NetServerMaxTickRate", "60.0");
            SetValue(editor, "IpDrv.TcpNetDriver", "LanServerMaxTickRate", "60");

            LogConfigured(fileName);
        }

        private void SetCustomKfGameSettings()
        {
            var fileName = Path.GetFileName(_paths.KfGameIniPath);

            LogConfiguring(fileName);
            
            var editor = new IniEditor(_paths.KfGameIniPath);
            SetValue(editor, "Engine.GameInfo", "bAdminCanPause", "true");
            SetValue(editor, "Engine.AccessControl", "AdminName", "defaultadmin");
            SetValue(editor, "Engine.AccessControl", "AdminPassword", "123455");

            _logger.Warning("WARNING!: do not forget change password using webAdmin or config file!");
            
            LogConfigured(fileName);
        }

        private void LogConfiguring(string fileName)
        {
            _logger.Information($"Configuring {fileName}:");
        }

        private void LogConfigured(string fileName)
        {
            _logger.Information($"{fileName} configured");
        }
        
        private void SetValue(IniEditor editor, string section, string key, string val)
        {
            editor.Write(section, key, val);
            _logger.Information($"    Set [{section}]{key}={val}");
        }
    }
}