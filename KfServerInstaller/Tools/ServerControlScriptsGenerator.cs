﻿using System.IO;
using KfServerInstaller.CommandBuilders;
using Serilog.Core;

namespace KfServerInstaller.Tools
{
    public sealed class ServerControlScriptsGenerator
    {
        private readonly PathHelper _paths;
        private readonly Logger _logger;

        public ServerControlScriptsGenerator(PathHelper paths, Logger logger)
        {
            _paths = paths;
            _logger = logger;
        }

        public void GenerateScripts()
        {
           CreateInstallScript();
           CreateUpdateScript();
           CreateOpenAdminScript();
           CreateRunServerDefaultScript();
           CreateRunServerEndlessScript();
        }

        private void CreateInstallScript()
        {
            var command = new SteamCmdArgsBuilder()
                .RunExecutable(_paths.SteamCmdPath)
                .LoginAnonymous()
                .InstallApp(_paths.KfServerFolder)
                .UpdateApp()
                .Exit()
                .BuildCommand();

            var script = CreateScript(command);

            SaveToFile("Install-Server.bat", script);

            _logger.Information("created Install-Server script");
        }

        private void CreateUpdateScript()
        {
            var command = new SteamCmdArgsBuilder()
                .RunExecutable(_paths.SteamCmdPath)
                .LoginAnonymous()
                .UpdateApp()
                .Exit()
                .BuildCommand();

            var script = CreateScript(command);

            SaveToFile("Update-Server.bat", script);

            _logger.Information("created Update-Server script");
        }

        private void CreateOpenAdminScript()
        {
            const string command = "start http://localhost:8080/ServerAdmin/";

            SaveToFile("Open-WebAdmin.bat", command);

            _logger.Information("created Open-WebAdmin script");
        }

        private void CreateRunServerDefaultScript()
        {
            var command = new KfServerArgumentsBuilder(_paths.KfServerExecutablePath, "KF-Nuked")
                .AddMaxPlayersMutator()
                .SetMaxPlayers(12)
                .SetMaxMonsters(120)
                .BuildCommand();

            var script = CreateScript(command);

            SaveToFile("Run-Server-Default.bat", script);

            _logger.Information("created Run-Server-Default script");
        }

        private void CreateRunServerEndlessScript()
        {
            var command = new KfServerArgumentsBuilder(_paths.KfServerExecutablePath, "KF-Nuked")
                .AddMaxPlayersMutator()
                .SetMaxPlayers(12)
                .SetMaxMonsters(120)
                .SetEndlessGameMode()
                .BuildCommand();

            var script = CreateScript(command);

            SaveToFile("Run-Server-Endless.bat", script);

            _logger.Information("created Run-Server-Endless script");
        }
        
        private void SaveToFile(string fileName, string content)
        {
            _paths.EnsureDirectoryExisting(_paths.ServerControlScriptsFolder);
            
            var filePath = Path.Combine(_paths.ServerControlScriptsFolder, fileName);

            File.WriteAllText(filePath, content);
        }

        private static string CreateScript(string command)
        {
            return $"start {command}";
        }
    }
}