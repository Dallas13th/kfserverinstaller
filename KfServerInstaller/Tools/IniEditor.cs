﻿using System.Runtime.InteropServices;
using System.Text;

namespace KfServerInstaller.Tools
{
    public sealed class IniEditor
    {
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section,
            string key,
            string val,
            string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
            string key,
            string def,
            StringBuilder retVal,
            int size,
            string filePath);

        public IniEditor(string iniFilePath)
        {
            IniFilePath = iniFilePath;
        }

        public string IniFilePath { get; }

        public void Write(string section, string key, string value)
        {
            WritePrivateProfileString(section, key, value.ToLower(), IniFilePath);
        }

        public string Read(string section, string key)
        {
            var builder = new StringBuilder(255);
            _ = GetPrivateProfileString(section, key, "", builder, 255, IniFilePath);
            return builder.ToString();
        }
    }
}