﻿using System;
using System.IO;

namespace KfServerInstaller.Tools
{
    public sealed class PathHelper
    {
        public PathHelper(string outputRootFolder, string steamCmdPath)
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            OutputRootFolder = Path.GetFullPath(outputRootFolder);
            ServerControlScriptsFolder = Path.Combine(OutputRootFolder, @"ServerControlScripts\");
            SteamCmdPath = steamCmdPath ?? Path.Combine(OutputRootFolder, "SteamCmd", "steamcmd.exe");

            TemporaryFolder = Path.Combine(currentDirectory, @"Temp\");
            SteamCmdZipPath = Path.Combine(TemporaryFolder, "steamcmd.zip");
            
            MaxPlayersMutatorSourcePath = Path.Combine(currentDirectory, "Mutators", "KFMutator.u");

            KfServerFolder = Path.Combine(OutputRootFolder, "Kf2Server");
            
            KfServerExecutablePath = Path.Combine(KfServerFolder, "Binaries", "Win64", "KFServer.exe");
            MaxPlayersMutatorDestionationPath = Path.Combine(KfServerFolder, "KFGame", "BrewedPC", "KFMutator.u");
            var kfConfigsFolder = Path.Combine(KfServerFolder, "KFGame", "Config");

            KfWebIniPath = Path.Combine(kfConfigsFolder, "KFWeb.ini");
            KfEngineIniPath = Path.Combine(kfConfigsFolder, "PCServer-KFEngine.ini");
            KfGameIniPath = Path.Combine(kfConfigsFolder, "PCServer-KFGame.ini");
        }

        public string OutputRootFolder { get; }

        public string SteamCmdClientUrl => "https://steamcdn-a.akamaihd.net/client/installer/steamcmd.zip";

        public string SteamCmdPath { get; }

        public string SteamCmdFolder => Path.GetDirectoryName(SteamCmdPath);

        public string SteamCmdZipPath { get; }
        
        public string TemporaryFolder { get; }

        public string KfServerFolder { get; }

        public string KfConfigsFolder { get; set; }

        public string KfWebIniPath { get; }

        public string KfEngineIniPath { get; }

        public string KfGameIniPath { get; }

        public string KfServerExecutablePath { get; }
        
        public string ServerControlScriptsFolder { get; }

        public string MaxPlayersMutatorSourcePath { get; }

        public string MaxPlayersMutatorDestionationPath { get; }

        public void EnsureDirectoryExisting(string path)
        {
            var directory = Path.GetDirectoryName(path);

            if (string.IsNullOrWhiteSpace(directory))
                throw new Exception("Couldn't find directory by path");

            if (Directory.Exists(directory))
                return;

            Directory.CreateDirectory(directory);
        }
    }
}