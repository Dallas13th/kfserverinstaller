﻿using CommandLine;

namespace KfServerInstaller.AppSettings
{
    [Verb("install", HelpText = @"Install KF2 server")]
    public sealed class InstallOptions
    {
        [Option('d', "directory", Required = true, HelpText = "Root directory for your server, and tools")]
        public string ServerRootDirectory { get; set; }

        [Option('s', "steamcmdpath", Required = false, HelpText = "Path to steamcmd, if you have already installed")]
        public string SteamCmdPath { get; set; }

    }
}