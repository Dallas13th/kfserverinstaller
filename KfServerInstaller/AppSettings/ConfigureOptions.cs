﻿using CommandLine;

namespace KfServerInstaller.AppSettings
{
    [Verb("configure", HelpText = @"Configure KF2 server after installation")]
    public sealed class ConfigureOptions
    {
        [Option('d', "directory", Required = true, HelpText = "Root directory for your server, which you specified when installing")]
        public string ServerRootDirectory { get; set; }
    }
}