﻿using System;
using System.Threading.Tasks;
using KfServerInstaller.CommandBuilders;
using KfServerInstaller.SteamCmd;
using KfServerInstaller.Tools;
using Serilog;
using Serilog.Core;

namespace KfServerInstaller.Installers
{
    public sealed class ServerInstaller : IDisposable
    {
        private readonly Logger _logger;
        private readonly PathHelper _paths;
        private readonly SteamCmdInstaller _cmdInstaller;
        private readonly SteamCmdRunner _steamCmdRunner;
        private readonly ServerControlScriptsGenerator _serverControlScriptsGenerator;
        private readonly MutatorInstaller _mutatorInstaller;

        public ServerInstaller(string serverRootDirectory, string steamCmdPath)
        {
            _logger = new LoggerConfiguration()
                .WriteTo
                .Console()
                .CreateLogger();

            _paths = new PathHelper(serverRootDirectory, steamCmdPath);
            _cmdInstaller = new SteamCmdInstaller(_paths, _logger);
            _steamCmdRunner = new SteamCmdRunner(_logger);
            _serverControlScriptsGenerator = new ServerControlScriptsGenerator(_paths, _logger);
            _mutatorInstaller = new MutatorInstaller(_paths, _logger);
        }

        public void Dispose()
        {
            _logger?.Dispose();
        }

        public async Task InstallAsync()
        {
            await _cmdInstaller.EnsureSteamCmdExistsAsync();
            
            var argsString = new SteamCmdArgsBuilder()
                .LoginAnonymous()
                .InstallApp(_paths.KfServerFolder)
                .UpdateApp()
                .Exit()
                .BuildCommand();

            _steamCmdRunner.RunProcess(_paths.SteamCmdPath, argsString);

            _mutatorInstaller.InstallMaxPlayersMutator();

            _serverControlScriptsGenerator.GenerateScripts();
            
            _logger.Information("KfServer successfulli installed!");
        }
    }
}