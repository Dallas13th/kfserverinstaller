﻿using System.IO;
using KfServerInstaller.Tools;
using Serilog.Core;

namespace KfServerInstaller.Installers
{
    public sealed class MutatorInstaller
    {
        private readonly PathHelper _paths;
        private readonly Logger _logger;

        public MutatorInstaller(PathHelper paths, Logger logger)
        {
            _paths = paths;
            _logger = logger;
        }

        public void InstallMaxPlayersMutator()
        {
            _logger.Information("Installing MaxPlayers mutator...");

            if (File.Exists(_paths.MaxPlayersMutatorDestionationPath))
            {
                _logger.Information("MaxPlayers mutator already exists");
                return;
            }

            _paths.EnsureDirectoryExisting(_paths.MaxPlayersMutatorDestionationPath);

            File.Copy(_paths.MaxPlayersMutatorSourcePath, _paths.MaxPlayersMutatorDestionationPath);

            _logger.Information("MaxPlayers mutator installed");
        }
    }
}