﻿using System;

namespace KfServerInstaller.Extensions
{
    public static class ExceptionExtensions
    {
        public static string ExpandInnerMessages(this Exception ex)
        {
            var resultMesage = string.Empty;
            var currentException = ex;

            var nestingLevel = 0;

            while (currentException != null)
            {
                resultMesage += "\n" + string.Empty.PadLeft(nestingLevel * 4) + currentException.Message;

                nestingLevel++;
                currentException = currentException.InnerException;
            }

            return resultMesage;
        }
    }
}