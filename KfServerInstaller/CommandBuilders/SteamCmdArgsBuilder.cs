﻿using System.Collections.Generic;

namespace KfServerInstaller.CommandBuilders
{
    public sealed class SteamCmdArgsBuilder
    {
        private readonly List<string> _commandsList;

        public SteamCmdArgsBuilder()
        {
            _commandsList = new List<string>();
        }

        public SteamCmdArgsBuilder RunExecutable(string executablePath)
        {
            _commandsList.Add(executablePath);

            return this;
        }

        public SteamCmdArgsBuilder LoginAnonymous()
        {
            _commandsList.Add("+login anonymous");

            return this;
        }

        public SteamCmdArgsBuilder InstallApp(string outputRootFolder)
        {
            _commandsList.Add($"+force_install_dir {outputRootFolder}");

            return this;
        }

        public SteamCmdArgsBuilder UpdateApp()
        {
            //232130 - ID of kf2 dedicated server in steam
            _commandsList.Add("+app_update 232130");

            return this;
        }

        public SteamCmdArgsBuilder Exit()
        {
            _commandsList.Add("+exit");

            return this;
        }

        public string BuildCommand()
        {
            return string.Join(" ", _commandsList);
        }
    }
}