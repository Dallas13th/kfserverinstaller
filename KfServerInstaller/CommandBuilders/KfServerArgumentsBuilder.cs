﻿using System.Collections.Generic;
using System.Linq;

namespace KfServerInstaller.CommandBuilders
{
    public sealed class KfServerArgumentsBuilder
    {
        private readonly string _executablePath;
        private readonly string _defaultMapName;
        private readonly List<string> _arguments;

        public KfServerArgumentsBuilder(string executablePath, string defaultMapName)
        {
            _executablePath = executablePath;
            _defaultMapName = defaultMapName;
            _arguments = new List<string>();
        }

        public KfServerArgumentsBuilder AddMaxPlayersMutator()
        {
            _arguments.Add("Mutator=KFMutator.KFMutator_MaxPlayersV2");

            return this;
        }

        public KfServerArgumentsBuilder SetMaxPlayers(int count = 6)
        {
            _arguments.Add($"MaxPlayers={count}");

            return this;
        }

        public KfServerArgumentsBuilder SetMaxMonsters(int count = 32)
        {
            _arguments.Add($"MaxMonsters={count}");

            return this;
        }

        public KfServerArgumentsBuilder SetEndlessGameMode()
        {
            _arguments.Add("Game=KFGameContent.KFGameInfo_Endless");

            return this;
        }

        public string BuildCommand()
        {
            var args = _arguments.Aggregate(string.Empty, (current, next) => current + "?" + next);

            var result = _executablePath + " " + _defaultMapName + args;

            return result;
        }
    }
}