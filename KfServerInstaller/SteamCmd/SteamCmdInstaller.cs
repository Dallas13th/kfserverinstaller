﻿using System;
using System.IO;
using System.IO.Compression;
using System.Net.Http;
using System.Threading.Tasks;
using KfServerInstaller.Tools;
using Serilog.Core;

namespace KfServerInstaller.SteamCmd
{
    public sealed class SteamCmdInstaller
    {
        private readonly PathHelper _paths;
        private readonly Logger _logger;

        public SteamCmdInstaller(PathHelper paths, Logger logger)
        {
            _paths = paths;
            _logger = logger;
        }

        public async Task EnsureSteamCmdExistsAsync()
        {
            _logger.Information("Checking for existing steamcmd...");

            if (File.Exists(_paths.SteamCmdPath))
            {
                _logger.Information("Found existing steamcmd.");
                return;
            }

            _logger.Information("Steamcmd has not been found.");
            _logger.Information("Downloading SteamCmd...");

            try
            {
                await DownloadAsync();

                _logger.Information("SteamCmd Successfully downloaded.");

                Unzip();
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);

                RemoveTempDir();

                throw;
            }

            RemoveTempDir();

            _logger.Information("SteamCmd installation completed");
        }

        public void RemoveTempDir()
        {
            if (!Directory.Exists(_paths.TemporaryFolder))
                return;

            _logger.Information("Removing temporary directory...");

            Directory.Delete(_paths.TemporaryFolder, true);

            _logger.Information("Temporary directory has been removed");
        }

        public void Unzip()
        {
            _logger.Information("Unpacking...");
            
            _paths.EnsureDirectoryExisting(_paths.SteamCmdFolder);

            ZipFile.ExtractToDirectory(_paths.SteamCmdZipPath, _paths.SteamCmdFolder);

            _logger.Information("Unpacking has been completed.");
        }

        public async Task DownloadAsync()
        {
            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage())
            {
                client.BaseAddress = new Uri(_paths.SteamCmdClientUrl, UriKind.Absolute);

                request.Method = HttpMethod.Get;

                var response = await client.SendAsync(request);

                if (!response.IsSuccessStatusCode)
                {
                    var stringError = await response.Content.ReadAsStringAsync();

                    throw new Exception($"steamcmd download http error. {response.StatusCode}\n{stringError}");
                }

                var content = await response.Content.ReadAsByteArrayAsync();

                _paths.EnsureDirectoryExisting(_paths.SteamCmdZipPath);

                File.WriteAllBytes(_paths.SteamCmdZipPath, content);
            }
        }
    }
}