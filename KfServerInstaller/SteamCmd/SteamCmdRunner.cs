﻿using System;
using System.Diagnostics;
using System.Text;
using ProcessTracker;
using Serilog.Core;

namespace KfServerInstaller.SteamCmd
{
    public sealed class SteamCmdRunner
    {
        private readonly Logger _logger;
        private Process _process;

        public SteamCmdRunner(Logger logger)
        {
            _logger = logger;
        }

        public void RunProcess(string executablePath, string arguments)
        {
            _logger.Information($"Starting steamcmd with args: {arguments}");

            _process = CreateProcess(executablePath, arguments);

            using (_process)
            {
                _process.Start();

                ChildProcessTracker.AddProcess(_process);
                
                _process.BeginOutputReadLine();
                _process.BeginErrorReadLine();

                _process.WaitForExit();
                
                if (_process.HasExited && _process.ExitCode != 0)
                {
                    throw new Exception($"Process {_process.Id} {_process.ProcessName} has exited with code {_process.ExitCode}");
                }
            }
        }
        
        private Process CreateProcess(string executablePath, string arguments)
        {
            var process = new Process
            {
                EnableRaisingEvents = true,

                StartInfo =
                {
                    FileName = executablePath,
                    Arguments = arguments,
                    CreateNoWindow = true,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    StandardErrorEncoding = Encoding.UTF8,
                    StandardOutputEncoding = Encoding.UTF8,
                    ErrorDialog = false
                }
            };

            process.OutputDataReceived += ProcessOnOutputDataReceived;
            process.ErrorDataReceived += ProcessOnErrorDataReceived;

            return process;
        }
        
        private void ProcessOnOutputDataReceived(object sender, DataReceivedEventArgs args)
        {
            if (string.IsNullOrWhiteSpace(args.Data))
                return;

            _logger.Information($"SteamCmd: {args.Data}");
        }

        private void ProcessOnErrorDataReceived(object sender, DataReceivedEventArgs args)
        {
            if (string.IsNullOrWhiteSpace(args.Data))
                return;

            _logger.Error($"SteamCmd: {args.Data}");
        }
    }
}