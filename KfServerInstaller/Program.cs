﻿using System;
using System.Threading.Tasks;
using CommandLine;
using KfServerInstaller.AppSettings;
using KfServerInstaller.Configurators;
using KfServerInstaller.Extensions;
using KfServerInstaller.Installers;

namespace KfServerInstaller
{
    public sealed class Program
    {
        public static void Main(string[] args)
        {
            var parsedResult = Parser.Default.ParseArguments<InstallOptions, ConfigureOptions>(args);

            var exitCode = parsedResult.MapResult((InstallOptions options) => InstallServer(options),
                (ConfigureOptions options) => ConfigureServer(options),
                errors => 1);

            Environment.Exit(exitCode);
        }

        private static int InstallServer(InstallOptions options)
        {
            try
            {
                InstallServerAsync(options).Wait();
                ReadUserInteraction();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ExpandInnerMessages());
                ReadUserInteraction();
                return 1;
            }

            return 0;
        }

        private static async Task InstallServerAsync(InstallOptions options)
        {
            using (var installer = new ServerInstaller(options.ServerRootDirectory, options.SteamCmdPath))
            {
                await installer.InstallAsync();
            }
        }

        private static int ConfigureServer(ConfigureOptions options)
        {
            using (var configurator = new KfGameConfigurator(options.ServerRootDirectory))
            {
                configurator.Configure();
            }

            ReadUserInteraction();
            return 0;
        }

        private static void ReadUserInteraction()
        {
            Console.Write("Press any key to exit.");
            Console.ReadKey();
        }
    }
}